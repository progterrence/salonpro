User-agent: * 
Disallow: /vendors/
Disallow: /cgi-bin/
Disallow: /admin/ 
Disallow: /images/
sitemap: https://salonist.io/sitemap.xml